public class Client
{
    public static void main(String[] args)
    {
        Vector a = new Vector(2, 3);
        Vector b = new Vector(3, 5);
        Vector c = new Vector(1, 2);

        Vector result = a.add(b).multiply(4).add(c.multiply(2));
    }
}
