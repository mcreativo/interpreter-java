public class Vector
{
    private double x;

    private double y;

    public Vector(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    public Vector add(Vector vector)
    {
        return new Vector(x + vector.x, y + vector.y);
    }

    public Vector multiply(double k)
    {
        return new Vector(x * k, y * k);
    }
}
